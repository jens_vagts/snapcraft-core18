# Snapcraft Core18

Docker image for building core18 based snaps on docker for `AMD64` **and** `ARM64` (`aarch64`). The `Dockerfile` is based on the official [Dockerfile(edge)](https://github.com/snapcore/snapcraft/blob/master/docker/edge.Dockerfile) from [Snapcraft Github project sources](https://github.com/snapcore/snapcraft).

The original snapcraft docker image `snapcore/snapcraft` does **not** support the base `core18`
and it does not support multi architecture/cross building of targeted `arm64` snaps!

This project build pipeline does deploy the built docker image on the docker hub as
[vagtsi/snapcraft-core18](https://hub.docker.com/repository/docker/vagtsi/snapcraft-core18) ready to be used in your own builds.

Use following command to build your ARM64 snap with your local docker environment:
- on a **AMD**64 machine: `sudo docker run -it --rm -w /mnt -v $PWD:/mnt vagtsi/snapcraft-core18 snapcraft --target-arch=arm64`
- on a **ARM**64 machine: `sudo docker run -it --rm -w /mnt -v $PWD:/mnt vagtsi/snapcraft-arm64 snapcraft`

## Additional sources used in this project
- [Snapcraft arm64 docker image](https://gitlab.com/jens_vagts/snapcraft-arm64)
- [Docker Engine on Intel Linux runs Arm Containers](https://blog.hypriot.com/post/docker-intel-runs-arm-containers/)

**Note**: This project will get obsolete as soon as the snapcraft team will create this docker image 
as requested already by users via development forum:
https://forum.snapcraft.io/t/snapcraft-pr-add-official-core18-docker-images/13731